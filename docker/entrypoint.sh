#!/bin/bash

# Проверяем значение переменной окружения SERVICE_NAME
if [ "$SERVICE_NAME" == "aiohttp" ]; then
    echo "Запуск сервиса 1"
    gunicorn main_app:test_app --bind 0.0.0.0:8000 --worker-class aiohttp.GunicornUVLoopWebWorker --timeout 0 --workers 4
elif [ "$SERVICE_NAME" == "grpc" ]; then
    # Запускаем второй сервис
    echo "Запуск сервиса 2"
    python /app/main_server_grpc.py
else
    echo "Неизвестный сервис: $SERVICE_NAME"
fi
