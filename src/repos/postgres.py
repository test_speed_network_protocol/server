import logging
import random
import string
from datetime import datetime as dt
from datetime import timedelta
from typing import Any

import pytz
from sqlalchemy import MetaData, String, cast, create_engine, delete, insert, select
from sqlalchemy.ext.asyncio import AsyncEngine, create_async_engine

from config import db_config
from utils.serialisations import json_deserializer, json_serializer

connection_str = f'{db_config.USER}:{db_config.PASSWORD}@{db_config.HOST}:{db_config.PORT}/{db_config.NAME}'

DATABASE_URL = f"postgresql+asyncpg://{connection_str}"


metadata_obj = MetaData()


class PostgresRepo:
    _engine: AsyncEngine
    _temp_list_strings: list[str]
    _logger: logging.Logger

    def __init__(self, engine: AsyncEngine) -> None:
        self._engine = engine
        self._logger = logging.getLogger(__name__)

    def create_table(self) -> None:
        from models.main import main_data

        engine_sync = create_engine(f'postgresql://{connection_str}')
        metadata_obj.create_all(engine_sync)

    async def generate_data(self, count_lines: int) -> None:
        from models.main import main_data

        batch = 1_000
        self._fill_temp_strings(batch)
        count_down = count_lines
        async with self._engine.connect() as connection:
            try:
                while count_down > 0:
                    print(f'count_down = {count_down}')
                    array_data = []
                    count = min(count_down, batch)
                    for _ in range(count):
                        array_data.append({'data': self._get_new_json()})
                    stmt = insert(main_data).values(array_data)
                    await connection.execute(stmt)
                    await connection.commit()
                    count_down -= count
            except Exception as ex:
                self._logger.exception(ex)

        self._temp_list_strings = []

    async def get_batch_data(self, from_index: int, batch_size: int = 10_000) -> list[dict[str, Any]]:
        from models.main import main_data

        try:
            async with self._engine.connect() as connection:
                stmt = (
                    select(main_data.c.id, cast(main_data.c.data, String).label('jsonb_as_string'))
                    .order_by(main_data.c.id)
                    .where(main_data.c.id > from_index)
                    .limit(batch_size)
                )
                result = await connection.execute(stmt)
                return [{'index': item.id, 'data': item.jsonb_as_string} for item in result.all()]
        except Exception as ex:
            self._logger.exception(ex)
            raise

    async def clear_table(self) -> None:
        from models.main import main_data

        async with self._engine.begin() as connection:
            stmt = delete(main_data)
            await connection.execute(stmt)

    def _fill_temp_strings(self, batch: int) -> None:
        self._temp_list_strings = []
        chars = string.ascii_letters + string.digits
        for _ in range(0, batch * 2):
            string_length = random.randint(0, 1_000)
            self._temp_list_strings.append(''.join(random.choice(chars) for _ in range(string_length)))

    def _get_new_json(self) -> dict[str, Any]:
        count_columns = 100
        list_type_column = ['int', 'str', 'date']
        dict_json = {}
        date_start = dt.now(pytz.UTC)
        for i in range(count_columns):
            type_column = random.choice(list_type_column)
            column_name = f'column_{i}'
            if type_column == 'int':
                dict_json[column_name] = random.randint(0, 1_000_000_000)
            elif type_column == 'str':
                dict_json[column_name] = random.choice(self._temp_list_strings)
            elif type_column == 'date':
                dict_json[column_name] = date_start - timedelta(seconds=random.randint(0, 1_000_000_000))

        return dict_json


repo_postgres: PostgresRepo | None = None


async def get_repo_postgres(pool_size: int) -> PostgresRepo:
    global repo_postgres
    if repo_postgres is None:
        engine_global = create_async_engine(
            DATABASE_URL,
            echo=True,
            json_deserializer=json_deserializer,
            json_serializer=json_serializer,
            pool_size=pool_size,
            max_overflow=10,
        )
        repo_postgres = PostgresRepo(engine=engine_global)
        repo_postgres.create_table()

    return repo_postgres
