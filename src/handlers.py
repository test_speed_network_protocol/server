import gzip
import io
from http import HTTPStatus

import fastavro
import orjson
import umsgpack
from aiohttp import web

from main_app import main_routes
from repos.postgres import get_repo_postgres
from utils.response_utility import get_response

POOL_SIZE = 20  # т.к. запуск идет в 4 процесса


@main_routes.get('/generate_data/{count_lines}')
async def generate_data(request: web.Request) -> web.Response:
    count_lines = int(request.match_info['count_lines'])
    repo_postgres = await get_repo_postgres(pool_size=POOL_SIZE)
    await repo_postgres.generate_data(count_lines=count_lines)

    return get_response(meta={'msg': f'Данные успешно сгенерированы count_lines = "{count_lines}"'})


@main_routes.post('/clear_data')
async def clear_data(request: web.Request) -> web.Response:
    repo_postgres = await get_repo_postgres(pool_size=POOL_SIZE)
    await repo_postgres.clear_table()

    return get_response(meta={'msg': 'Данные успешно удалены'})


@main_routes.get('/get_data/json')
async def get_data_json(request: web.Request) -> web.Response:
    batch_size = request.query.get('batch_size', 1_000)
    start_index = request.query.get('start_index', 0)
    repo_postgres = await get_repo_postgres(pool_size=POOL_SIZE)
    data = await repo_postgres.get_batch_data(from_index=int(start_index), batch_size=int(batch_size))
    start_index_next_page = 0
    if len(data) > 0:
        start_index_next_page = data[-1]['index']

    return get_response(
        data=data,
        meta={'start_index_next_page': start_index_next_page, 'control_value': f'Порция данных {start_index}'},
    )


avro_schema = {
    "type": "record",
    "name": "MyData",
    "fields": [
        {
            "name": "meta",
            "type": {
                "type": "record",
                "name": "Meta",
                "fields": [
                    {"name": "start_index_next_page", "type": "int"},
                    {"name": "control_value", "type": "string"},
                ],
            },
        },
        {
            "name": "data",
            "type": {
                "type": "array",
                "items": {
                    "type": "record",
                    "name": "DataItem",
                    "fields": [{"name": "index", "type": "int"}, {"name": "data", "type": "string"}],
                },
            },
        },
    ],
}

avro_schema_parsed = fastavro.parse_schema(avro_schema)


@main_routes.get('/get_data/avro')
async def get_data_avro(request: web.Request) -> web.Response:
    batch_size = request.query.get('batch_size', 1_000)
    start_index = request.query.get('start_index', 0)
    repo_postgres = await get_repo_postgres(pool_size=POOL_SIZE)
    data = await repo_postgres.get_batch_data(from_index=int(start_index), batch_size=int(batch_size))
    start_index_next_page = 0
    if len(data) > 0:
        start_index_next_page = data[-1]['index']
    body = {
        'data': data,
        'meta': {'start_index_next_page': start_index_next_page, 'control_value': f'Порция данных {start_index}'},
    }

    avro_data = None
    with io.BytesIO() as out:
        fastavro.writer(out, avro_schema_parsed, [body])
        avro_data = out.getvalue()

    return web.Response(body=avro_data, headers={"Content-Type": "application/avro"}, status=HTTPStatus.OK)


@main_routes.get('/get_data/json_gzip')
async def get_data_json_gzip(request: web.Request) -> web.Response:
    batch_size = request.query.get('batch_size', 1_000)
    start_index = request.query.get('start_index', 0)
    repo_postgres = await get_repo_postgres(pool_size=POOL_SIZE)
    data = await repo_postgres.get_batch_data(from_index=int(start_index), batch_size=int(batch_size))
    start_index_next_page = 0
    if len(data) > 0:
        start_index_next_page = data[-1]['index']
    body = {
        'data': data,
        'meta': {'start_index_next_page': start_index_next_page, 'control_value': f'Порция данных {start_index}'},
    }

    obj_as_bytes = orjson.dumps(body)
    body = gzip.compress(obj_as_bytes, compresslevel=5)
    return web.Response(body=body, headers={"Content-Encoding": "gzip"}, status=HTTPStatus.OK)


@main_routes.get('/get_data/msgpack')
async def get_data_msgpack(request: web.Request) -> web.Response:
    batch_size = request.query.get('batch_size', 1_000)
    start_index = request.query.get('start_index', 0)
    repo_postgres = await get_repo_postgres(pool_size=POOL_SIZE)
    data = await repo_postgres.get_batch_data(from_index=int(start_index), batch_size=int(batch_size))
    start_index_next_page = 0
    if len(data) > 0:
        start_index_next_page = data[-1]['index']
    body = {
        'data': data,
        'meta': {'start_index_next_page': start_index_next_page, 'control_value': f'Порция данных {start_index}'},
    }

    obj_as_bytes = orjson.dumps(body)
    body = umsgpack.packb(obj=obj_as_bytes)
    return web.Response(body=body, headers={"Content-Encoding": " identity"}, status=HTTPStatus.OK)


def add_all_handlers() -> None:
    # данная функция нужна, чтобы при ее импорте зарегались все router
    pass
