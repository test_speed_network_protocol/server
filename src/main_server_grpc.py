import logging
import sys
from aiohttp import web

from grpc_test import grpc_server


logger = logging.getLogger("main")


def init() -> web.Application:
    # logger.info(f"action=init_app, {config}")
    app = web.Application(middlewares=[], debug=True)

    app.cleanup_ctx.append(grpc_server.grpc_server_ctx)

    return app


if __name__ == "__main__":
    access_log_format = "request: %a %r %s %b %Tf %s %b"
    app = init()
    try:
        web.run_app(app, port=50052, access_log_format=access_log_format, print=lambda *args: None)
    except Exception:
        logger.exception("action=run_app, status=failed")
        sys.exit(1)
