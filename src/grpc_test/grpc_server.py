import asyncio
import logging
from concurrent.futures import ThreadPoolExecutor
from typing import Any, AsyncGenerator, Awaitable, Callable

import grpc
from aiohttp import web

from config import grpc_port
from grpc_test import test_pb2, test_pb2_grpc
from repos.postgres import get_repo_postgres

logger = logging.getLogger(__name__)

POOL_SIZE = 25


class Service1(test_pb2_grpc.DataService):
    def __init__(self, app: web.Application) -> None:
        self.app = app

    async def GetData(self, request: test_pb2.DataRequest, context: grpc.aio.ServicerContext) -> test_pb2.DataStructure:
        batch_size = request.batch_size
        start_index = request.start_index
        repo_postgres = await get_repo_postgres(pool_size=POOL_SIZE)
        data = await repo_postgres.get_batch_data(from_index=int(start_index), batch_size=int(batch_size))
        start_index_next_page = 0
        if len(data) > 0:
            start_index_next_page = data[-1]['index']
        body = {
            'data': data,
            'meta': {'start_index_next_page': start_index_next_page, 'control_value': f'Порция данных {start_index}'},
        }

        return self._convert_body_to_msg(body=body)

    def _convert_body_to_msg(self, body: dict[str, Any]) -> test_pb2.DataStructure:
        meta = test_pb2.Meta(
            start_index_next_page=body['meta']['start_index_next_page'], control_value=body['meta']['control_value']
        )
        data_items = [test_pb2.DataItem(index=item['index'], data=item['data']) for item in body['data']]
        return test_pb2.DataStructure(meta=meta, data=data_items)


def _init(app: web.Application, listen_addr: str) -> grpc.aio.Server:
    MAX_MESSAGE_LENGTH = -1
    server = grpc.aio.server(
        ThreadPoolExecutor(max_workers=100),
        options=[
            ('grpc.max_send_message_length', MAX_MESSAGE_LENGTH),
            ('grpc.max_receive_message_length', MAX_MESSAGE_LENGTH),
        ],
    )
    server.add_insecure_port(listen_addr)
    test_pb2_grpc.add_DataServiceServicer_to_server(Service1(app), server)
    return server


async def _start_grpc_server(server: grpc.aio.Server) -> None:
    await server.start()
    await server.wait_for_termination()


async def grpc_server_ctx(app: web.Application) -> AsyncGenerator:
    listen_addr = f"[::]:{grpc_port}"

    server = _init(app, listen_addr)
    task = asyncio.create_task(_start_grpc_server(server))
    logger.info(f"action=init_grpc_server, address={listen_addr}")

    yield

    await server.stop(grace=None)
    task.cancel()
    await task
