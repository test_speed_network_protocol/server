import logging

logger = logging.getLogger(__name__)


async def error_middleware(app, handler):
    async def middleware(request):
        try:
            return await handler(request)
        except Exception as e:
            logger.getChild(handler.__name__).exception('Исключение в handler', exc_info=e)
            raise

    return middleware
