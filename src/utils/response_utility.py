from http import HTTPStatus
from typing import Any
from aiohttp import web


def get_response(
    data: dict[str, Any] | None = None, meta: dict[str, Any] | None = None, status: HTTPStatus = HTTPStatus.OK
) -> web.Response:
    return web.json_response({'data': data or {}, 'meta': meta or {}}, status=status)
