from typing import Any

import orjson


def json_deserializer(input_data: str) -> dict[str, Any]:
    return orjson.loads(input_data.encode('utf-8'))


def json_serializer(input_data: dict[str, Any]) -> str:
    return orjson.dumps(input_data).decode('utf-8')
