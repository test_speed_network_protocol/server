from functools import wraps
import logging


def log_exceptions(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as exc_info:
            logger = logging.getLogger(func.__name__)
            logger.exception("Exception ", exc_info=exc_info)
            raise

    return wrapper
