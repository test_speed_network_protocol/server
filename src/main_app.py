import pathlib
from logging.config import dictConfig

from aiohttp import web

from middleware.errors_logger import error_middleware

main_routes = web.RouteTableDef()


def _log_config() -> None:
    _log_file = pathlib.Path(__file__).parents[1] / 'logs'
    _log_file.mkdir(parents=True, exist_ok=True)
    _log_file = _log_file / 'log_exception.log'
    dictConfig(
        {
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'default': {'format': '[%(asctime)s: %(levelname)s] %(message)s', 'datefmt': '%Y-%m-%d %H:%M:%S'}
            },
            'handlers': {
                'console': {
                    'class': 'logging.StreamHandler',
                    'level': 'DEBUG',
                    'formatter': 'default',
                    'stream': 'ext://sys.stdout',
                },
                'file_exception': {
                    'level': 'ERROR',
                    'class': 'logging.FileHandler',
                    'filename': str(_log_file),
                    'formatter': 'default',
                },
            },
            'loggers': {
                'root': {'level': 'DEBUG', 'handlers': ['console', 'file_exception']},
                'aiohttp.server': {
                    'handlers': ['console', 'file_exception'],
                    'level': 'DEBUG',
                    'propagate': False,
                },
                'aiohttp.access': {
                    'handlers': ['console', 'file_exception'],
                    'level': 'DEBUG',
                    'propagate': False,
                },
                'sqlalchemy': {
                    'handlers': ['console', 'file_exception'],
                    'level': 'DEBUG',
                    'propagate': False,
                },
            },
        }
    )


async def test_app():
    list_middleware = [error_middleware]
    _log_config()

    app = web.Application(middlewares=list_middleware, debug=True)
    # logging.basicConfig(level=logging.DEBUG)
    from handlers import add_all_handlers

    add_all_handlers()
    app.router.add_routes(main_routes)
    return app
