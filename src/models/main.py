from sqlalchemy import (
    Column,
    Integer,
    Table,
)
from sqlalchemy.dialects.postgresql import JSONB

from repos.postgres import metadata_obj

main_data = Table(
    'main_data',
    metadata_obj,
    Column('id', Integer, primary_key=True),
    Column('data', JSONB),
)
