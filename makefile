start_grpc:
	python ./src/main_server_grpc.py

start_aiohttp:
	gunicorn main_app:test_app --bind localhost:8000 --worker-class aiohttp.GunicornUVLoopWebWorker --timeout 0 --workers 4

